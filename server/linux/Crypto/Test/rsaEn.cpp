#include <iostream>
#include <fstream>
#include <vector>
#include <botan/pubkey.h>
#include <botan/botan.h>
#include <botan/rsa.h>

using namespace Botan;
int main(int argc,char **argv){
	Botan::LibraryInitializer init;
	AutoSeeded_RNG rng;
	std::ofstream ot(argv[4],std::ios::binary);
	std::auto_ptr<X509_PublicKey> key(X509::load_key(argv[1]));
	std::auto_ptr<PKCS8_PrivateKey> pk(PKCS8::load_key(argv[2],rng));
	RSA_PublicKey *pubk = dynamic_cast<RSA_PublicKey*>(key.get());
	RSA_PrivateKey *privk = dynamic_cast<RSA_PrivateKey*>(pk.get());
	PK_Encryptor_EME enc(*pubk,"EME-PKCS1-v1_5");
	PK_Decryptor_EME dec(*privk,"EME-PKCS1-v1_5");
	Botan::byte msg[5];
	memcpy(msg,argv[3],5);
	std::vector<Botan::byte> ciphertext = enc.encrypt(msg,sizeof(msg),rng);
	std::string str(ciphertext.begin(),ciphertext.end());
	Botan::byte m[str.size()];
	memcpy(m,str.data(),str.size());
	
	Botan::secure_vector<Botan::byte> plain = dec.decrypt(m,sizeof(m));
	std::cout<<str<<std::endl;
	std::string pt(plain.begin(),plain.end());
	ot<<str;
	ot.close();
	return 0;
}
