#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <botan/pubkey.h>
#include <botan/botan.h>
#include <botan/rsa.h>

using namespace Botan;
int main(int argc,char **argv){
	Botan::LibraryInitializer init;
	AutoSeeded_RNG rng;
	std::auto_ptr<PKCS8_PrivateKey> pk(PKCS8::load_key(argv[2],rng));
	RSA_PrivateKey *privk = dynamic_cast<RSA_PrivateKey*>(pk.get());
	PK_Decryptor_EME dec(*privk,"EME-PKCS1-v1_5");
	std::ifstream ci(argv[1],std::ios::binary);
	ci.seekg(0,std::ios_base::end);
	int s = ci.tellg();
	ci.seekg(0,std::ios_base::beg);
	std::vector<char> vecd(s);
	ci.read(&vecd[0],s);
	std::string str(vecd.begin(),vecd.end());
	Botan::byte m[str.size()];
	memcpy(m,str.data(),str.size());
	Botan::secure_vector<Botan::byte> plain = dec.decrypt(m,sizeof(m));
	std::cout<<str<<std::endl;
	std::string pt(plain.begin(),plain.end());
	std::cout<<pt<<std::endl;
	
	return 0;
}
