#include <iostream>
#include "CryptoAES.h"
#include <fstream>
#include <vector>

using namespace std;
int main(int argc,char** argv){
	CryptoAES *crypto = new CryptoAES();
	std::ifstream ciph(argv[1],ios::binary);
	ciph.seekg(0,ios::end);
	size_t len = ciph.tellg();
	ciph.seekg(0,ios::beg);
	std::vector<char> ciphV(len);
	ciph.read(&ciphV[0],len);
	ciph.close();
	char* packet = strdup(string(ciphV.begin(),ciphV.end()).data());
	char* plain = crypto->AESDecrypt(packet);
	cout<<plain<<endl;
}
