#include <iostream>
#include <fstream>
#include <vector>
#include <botan/pubkey.h>
#include <botan/botan.h>
#include <botan/rsa.h>
#include "CryptoRSA.h"
using namespace Botan;
CryptoRSA::CryptoRSA(){
        privateKeyStr = "private.pem";
        publicKeyStr = "public.pem";
	char* privKbuff = strdup(privateKeyStr.c_str());
	char* pubKbuff = strdup(publicKeyStr.c_str());
        privateKey.reset(PKCS8::load_key(privKbuff,rng));
        publicKey.reset(X509::load_key(pubKbuff));
}
CryptoRSA::~CryptoRSA(){}
char* CryptoRSA::RSADecrypt(char* packet,size_t len,int* newsize){
	RSA_PrivateKey *privk = dynamic_cast<RSA_PrivateKey*>(privateKey.get());
	PK_Decryptor_EME dec(*privk,"EME-PKCS1-v1_5");
	Botan::byte m[len];
	memcpy(m,packet,len);
	Botan::secure_vector<Botan::byte> plain = dec.decrypt(m,sizeof(m));
	std::string pt(plain.begin(),plain.end());
	*newsize = pt.size();
	char* decryptedPacket = strdup(pt.data());
	return decryptedPacket;
	
}
char* CryptoRSA::RSAEncrypt(char* packet,size_t len,int* newsize){
	RSA_PublicKey *pubk = dynamic_cast<RSA_PublicKey*>(publicKey.get());
	PK_Encryptor_EME enc(*pubk,"EME-PKCS1-v1_5");
	Botan::byte msg[len];
	memcpy(msg,packet,len);
	std::vector<Botan::byte> ciphertext = enc.encrypt(msg,sizeof(msg),rng);
	std::string str(ciphertext.begin(),ciphertext.end());
	*newsize = str.size();
	char* encryptedPacket = strdup(str.data());
	return encryptedPacket;
}
