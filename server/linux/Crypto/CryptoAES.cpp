#include <iostream>
#include <fstream>
#include <vector>
#include "CryptoAES.h"
#include <string.h>
#include <memory.h>
#include <botan/botan.h>
CryptoAES::CryptoAES(){
	std::ifstream keyIn("keyfile");
	std::ifstream ivIn("ivfile");

	keyIn.seekg(0,std::ios::end);
	size_t keyLen = keyIn.tellg();
	keyIn.seekg(0,std::ios::beg);

	ivIn.seekg(0,std::ios::end);
	size_t ivLen = ivIn.tellg();
	ivIn.seekg(0,std::ios::beg);

	std::vector<char> keyVec(keyLen);
	keyIn.read(&keyVec[0],keyLen);
	keyIn.close();

	std::vector<char> ivVec(ivLen);
	ivIn.read(&ivVec[0],ivLen);
	ivIn.close();

	std::string keyStr = std::string(keyVec.begin(),keyVec.end());
	std::string ivStr = std::string(ivVec.begin(),ivVec.end());

	Botan::SymmetricKey mKey(keyStr);
	Botan::InitializationVector mIv(ivStr);
	this->key = mKey;
	this->iv = mIv;
}
CryptoAES::~CryptoAES(){}
char* CryptoAES::AESEncrypt(char* packet,int packetLength,int* len){
	/*Botan::Pipe encryptionPipe(Botan::get_cipher("AES-128/CFB/NoPadding",key,iv,Botan::ENCRYPTION));	
	Botan::byte plainByte[packetLength];
	memcpy(plainByte,packet,packetLength);
	encryptionPipe.process_msg(plainByte,packetLength);
	Botan::secure_vector<Botan::byte> encryptedBytes = encryptionPipe.read_all();
	*len = packetLength;//Assumption that CFB will do no padding
	char* encryptedPacket = (char*)malloc(*len);
	memcpy(encryptedPacket,&encryptedBytes[0],*len);
*/	*len = packetLength;
	return packet;
}
char* CryptoAES::AESDecrypt(char* packet,int packetLength,int* len){
	/*Botan::Pipe decryptionPipe(Botan::get_cipher("AES-128/CFB/NoPadding",key,iv,Botan::DECRYPTION));
	Botan::byte encryptedByte[packetLength];
	memcpy(encryptedByte,packet,packetLength);
	decryptionPipe.process_msg(encryptedByte,packetLength);
	Botan::secure_vector<Botan::byte> decryptedBytes = decryptionPipe.read_all();
	*len = packetLength;//Under the assumption that CFB will do no padding
	char* decryptedPacket = (char*)malloc(decryptedBytes.size());
	memcpy(decryptedPacket,&decryptedBytes[0],decryptedBytes.size());
	*/
	*len = packetLength;
	return packet;
}
char* CryptoAES::Base64Encode(char* packet,size_t len,int* retLen){
	Botan::byte pack[len];
	memcpy(pack,packet,len);
	Botan::Pipe encodePipe(new Botan::Base64_Encoder);
	encodePipe.process_msg(pack,len);
	Botan::SecureVector<Botan::byte> encodedBytes = encodePipe.read_all();
	char* encodedPacket = (char*)malloc(encodedBytes.size());
	memcpy(encodedPacket,&encodedBytes[0],encodedBytes.size());
	*retLen = encodedBytes.size();
	return encodedPacket;
}
