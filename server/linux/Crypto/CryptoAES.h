#ifndef CRYPTOAES_H
#define CRYPTOAES_H 

#include <botan/botan.h>
class CryptoAES{
	public:
		CryptoAES();
		virtual ~CryptoAES();
		char* AESEncrypt(char* packet,int,int*);
		char* AESDecrypt(char* packet,int,int*);
		char* Base64Encode(char*,size_t,int*);
	private:
		Botan::SymmetricKey key;
		Botan::InitializationVector iv;

};
#endif
