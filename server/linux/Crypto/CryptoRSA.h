#include <iostream>
#include <fstream>
#include <vector>
#include <botan/pubkey.h>
#include <botan/botan.h>
#include <botan/rsa.h>

using namespace Botan;
class CryptoRSA{
        public:
                CryptoRSA();
                virtual ~CryptoRSA();
                char* RSADecrypt(char* packet,size_t len,int*);
                char* RSAEncrypt(char* packet,size_t len,int*);
        private:
                std::unique_ptr<PKCS8_PrivateKey> privateKey;
                std::unique_ptr<X509_PublicKey> publicKey;
                std::string privateKeyStr;
                std::string publicKeyStr;
                Botan::LibraryInitializer init;
	        AutoSeeded_RNG rng;
	        
	        
};
