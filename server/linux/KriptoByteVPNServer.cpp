#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <memory.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>

#ifdef __linux__

#include <net/if.h>
#include <linux/if_tun.h>

#include "KComp/Compressor.hpp"
#include "Crypto/CryptoAES.h"

static int getOsTunnel(char *name){
    int interface = open("/dev/net/tun", O_RDWR | O_NONBLOCK);
    ifreq ifr;
    memset(&ifr, 0, sizeof(ifr));
    ifr.ifr_flags = IFF_TUN | IFF_NO_PI;
    strncpy(ifr.ifr_name, name, sizeof(ifr.ifr_name));
    if (ioctl(interface, TUNSETIFF, &ifr)) {
        perror("Cannot get TUN interface");
        exit(1);
    }
    return interface;
}

#else
#endif

static int getVpnClient(char *port, char *authKey){
    // UDP IPV6 Socket Setup
    int tunnel = socket(AF_INET6, SOCK_DGRAM, 0);
    int flag = 1;
    setsockopt(tunnel, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(flag));
    flag = 0;
    setsockopt(tunnel, IPPROTO_IPV6, IPV6_V6ONLY, &flag, sizeof(flag));
    sockaddr_in6 addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin6_family = AF_INET6;
    addr.sin6_port = htons(atoi(port));
    while (bind(tunnel, (sockaddr *)&addr, sizeof(addr))) {
        if (errno != EADDRINUSE) {
            return -1;
        }
        usleep(100000);
    }

    char packet[1024];
    socklen_t addrlen;
    do {
        addrlen = sizeof(addr);
        int n = recvfrom(tunnel, packet, sizeof(packet), 0,(sockaddr *)&addr, &addrlen);
        if (n <= 0) {
            return -1;
        }
        packet[n] = 0;
    } while (packet[0] != 0 || strcmp(authKey, &packet[1]));
        connect(tunnel, (sockaddr *)&addr, addrlen);
    return tunnel;
}
inline void fillPacket(char *compressedPacket,std::string compressedPacketStr){
	memcpy(compressedPacket,compressedPacketStr.data(),compressedPacketStr.size());
}
inline void fillControl(char *compressedPacket,char* packet,int len){
	memcpy(compressedPacket,packet,len);
}
void printPacket(char* packet, size_t len){
	for(int i=0;i<len;++i){
		std::cout<<packet[i];
	}
	std::cout<<""<<std::endl;
}
//-----------------------------------------------------------------------------

int main(int argc, char **argv){
    char confirmationKey[] = "initialized";
    CryptoAES *cryptoAes = new CryptoAES();
    int interface = getOsTunnel(argv[1]);
    int tunnel;
    while ((tunnel = getVpnClient(argv[2], argv[3])) != -1) {
        printf("%s: Here comes a new tunnel\n", argv[1]);
        fcntl(tunnel, F_SETFL, O_NONBLOCK);
	int ckLen,encodedCkLen;
	char* encryptedCK = cryptoAes->AESEncrypt(confirmationKey,11,&ckLen);
    char* encryptedEncodedCK = cryptoAes->Base64Encode(encryptedCK, ckLen, &encodedCkLen);
        // Send the confirmation several times in case of packet loss.
        for (int i = 0; i < 3; ++i) {
		std::cout<<"CK: "<<encodedCkLen<<std::endl;
            send(tunnel, encryptedEncodedCK, encodedCkLen, MSG_NOSIGNAL);
        }
        char packet[32767];
	//0->receiving
	//1->sending
        int ioSwitch = 0;
        while (true) {
            bool idle = true;
	    /*Call Compressor*/
            int length = read(interface, packet, sizeof(packet));
	        int encryptedLen,encodedLen;
        if (length > 0) {
		  std::cout<<"[*]Sending Packet"<<std::endl;
		    /*Packet Compression*/
		    Compressor *compressor = new Compressor(packet,nullptr);
		    compressor->buffSize = length;
		    std::string compressedPacket = compressor->Deflate();
		
       if(length > compressedPacket.length()){
	    char* compressedPBuff = (char*)malloc(compressedPacket.size());
	    memcpy(compressedPBuff,compressedPacket.data(),compressedPacket.size());
		    /*Encrypt Compressed Packet: AES-128/CFB*/
            char* encryptedPacket = cryptoAes->AESEncrypt(compressedPBuff,compressedPacket.length(),&encryptedLen);
            /*Encode Compressed Encrypted Packet: Base64*/
            char* compressedEncryptedEncodedPacket = cryptoAes->Base64Encode(encryptedPacket,encryptedLen,&encodedLen);
		std::cout<< "O: "<<length<<"PEEC: "<<encodedLen<<std::endl;	
		char *compressedPacketWH = (char*)malloc(encodedLen+1);//1 for header
			compressedPacketWH[0] = ':';
			fillPacket(&compressedPacketWH[1],compressedEncryptedEncodedPacket);
			printPacket(compressedPacketWH,encodedLen+1);
			send(tunnel, compressedPacketWH,encodedLen+1, MSG_NOSIGNAL);
			std::cout<<"CnE: "<<encodedLen<<std::endl;
		}else{
            /*Encrypt Raw Packet*/
			char* encryptedPacketNC = cryptoAes->AESEncrypt(packet,length,&encryptedLen);
            /*Encode Encrypted Packet*/
            char* encodedEncryptedPacketNC = cryptoAes->Base64Encode(encryptedPacketNC,encryptedLen,&encodedLen);
			send(tunnel, encodedEncryptedPacketNC, encodedLen, MSG_NOSIGNAL);
			printPacket(encodedEncryptedPacketNC,encodedLen);
			std::cout<<"E: "<<encodedLen<<std::endl;
		}
		idle = false;
                //switch to sending.
                if (ioSwitch < 1) {
                    ioSwitch = 1;
                }
            }

            // Read the incoming packet from the tunnel.
            length = recv(tunnel, packet, sizeof(packet), 0);

            int decryptedLength;
	    if (length == 0) {
                break;
            }
        if (length > 0) {
		  char* compactPacketRX = (char*)malloc(length);
		  memcpy(compactPacketRX,packet,length);
                /*Packet Decryption*/
//--------------------------------------------------------------------------------------------------
		  char* decryptedPacket = cryptoAes->AESDecrypt(compactPacketRX,length,&decryptedLength);
		  if (decryptedPacket[0] != 0 && decryptedPacket[0] != 'c') {
		   std::cout<<"[*]Got Raw Packet: ";
//		   printPacket(decryptedPacket,decryptedLength);
                   int wr = write(interface, decryptedPacket, decryptedLength);
		std::cout<<"[***] Wrote: "<<wr<<std::endl;
                }else if(decryptedPacket[0] == 'c'){

			/*Packet Decompression*/
//---------------------------------------------------------------------------------------------------
			std::cout<<"[*]Got Compressed Packet"<<std::endl;
			Compressor *decompression = new Compressor();//Inflation Mode
			std::string decompressedPacket = decompression->Inflate(&decryptedPacket[1],length-1);
			int decompressedSize = decompression->decompressedSize;
//----------------------------------------------------------------------------------------------------
			int numwr = write(interface,decompressedPacket.data(),decompressedSize);
		}
                idle = false;

                //switch to receiving.
                if (ioSwitch > 0) {
                    ioSwitch = 0;
                }
            }
	    /*Seriously Dont touch any magic numbers :)*/
            // fraction of time to avoid busy looping.
            if (idle) {
                usleep(100000);

                ioSwitch += (ioSwitch > 0) ? 100 : -100;

                if (ioSwitch < -16000) {
                    // Send empty control messages.
                    packet[0] = 0;
		    packet[1] = 1;
		    int controlLen;
		    char* encryptedControl = cryptoAes->AESEncrypt(packet,2,&controlLen);
		    std::cout<<"Control Length: "<<controlLen<<std::endl;
		    for (int i = 0; i < 3; ++i) {
			char* encryptedControlPacket = (char*)malloc(controlLen+1);
			encryptedControlPacket[0] = 0;
			fillControl(&encryptedControlPacket[1],encryptedControl,controlLen);
                        send(tunnel, encryptedControlPacket, controlLen+1, MSG_NOSIGNAL);
                    }

                    // Switch to sending.
                    ioSwitch = 1;
                }

                // We are sending for a long time but not receiving.
                if (ioSwitch > 20000) {
                    break;
                }
            }
        }
        printf("%s: The tunnel is broken\n", argv[1]);
        close(tunnel);
    }
    perror("Cannot create tunnels");
    exit(1);
}
