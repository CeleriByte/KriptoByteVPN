#include <iostream>
#include <fstream>
#include <botan/botan.h>

using namespace Botan;
int main(void){
	Botan::LibraryInitializer init;
	Botan::AutoSeeded_RNG rng;
	Botan::SymmetricKey symKey(rng,16);
	Botan::InitializationVector iV(rng,16);
	std::ofstream keyfile("keyfile",std::ios::binary);
	std::ofstream ivfile("ivfile",std::ios::binary);
	keyfile<<symKey.OctetString::as_string();
	ivfile<<symKey.OctetString::as_string();
	keyfile.close();
	ivfile.close();
}
