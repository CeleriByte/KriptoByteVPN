#include <iostream>
#include <fstream>
#include <string>
#include <botan/botan.h>
#include <botan/rsa.h>

//Generates Public and Private Key

using namespace Botan;
int main(int argc,char **argv){
	int bits = 1024;
	std::ofstream out(argv[1],std::ios::out | std::ios::binary);
	std::ofstream out2(argv[2],std::ios::out | std::ios::binary);
	Botan::LibraryInitializer init;
	AutoSeeded_RNG rng;
	Botan::RSA_PrivateKey key(rng,bits);
	out<<X509::PEM_encode(key);
	out2<<PKCS8::PEM_encode(key);
	out.close();
	out2.close();
	return 0;
}
