#include <iostream>
#include <string.h>
#include "Compressor.hpp"
using namespace std;
int main(int argc,char **argv){
	int strl = strlen(argv[1]);
	cout<<strl<<endl;
	string arg(argv[1]);
	Compressor *compr = new Compressor(arg,nullptr);
	char* compressed = compr->Deflate();
	string compressedStr(compressed);
	std::cout<<compressedStr<<endl;
	int cSize = compressedStr.size();
	char* decomp = compr->Inflate(compressed,cSize);
	cout<<decomp<<endl;
	return 0;
}