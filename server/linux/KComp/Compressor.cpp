#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include "Compressor.hpp"
#include "boost/pool/pool.hpp"

#define ALLOC_CHUNK 132000;
using namespace std;
/*
 * CeleriByte Developer Network
 * Coder: Eric Ziscky
 * Tested: Yes - Passed
 * Date Tested: 5/04/2015
 *
 * */
//Notes
/*
 * For the deflate algorithm the user of the class is responsible for the changing of the checksums between
 * adler32 and CRC32 to determine compatibility with gzip
 * Other algorithms can and will be added after testing
 * */

Compressor::Compressor(char* subjectData,char *filename,int compressionLevel,int algorithm){
	if(this->errorCheck(subjectData,filename) < 0){
		//handle exception
	}
	switch(algorithm){
	case ALGORITHM_DEFLATE://Deflate algorithm - default option
		DEFLATE_ENABLE = 1;
	}
	this->compressionLevel = compressionLevel;
	
}
Compressor::Compressor(){}//default constructor
Compressor::~Compressor(){}

int Compressor::errorCheck(char *subjectData,char *filename){
        if(filename == nullptr){
	        this->subjectData = subjectData;
		this->filename = nullptr;
	}
	if(subjectData == nullptr){
		this->filename = filename;
		
	}
	if(filename == nullptr && subjectData == nullptr){
		return -1;
	}
	return 0;
}


std::string Compressor::Deflate(int compressorWindowBits,int compressorMemoryLevel,int compressorMethod){
	if(subjectData == "" || this->DEFLATE_ENABLE != 1){
		//throw exception
	}
	z_stream *zstream = new z_stream();	
	zstream->next_in = reinterpret_cast<Bytef*>(subjectData);
	zstream->avail_in = buffSize;
	int INIT_STATUS = deflateInit2(zstream,this->levelDecoder(compressionLevel),Z_DEFLATED,compressorWindowBits,compressorMemoryLevel,compressorMethod);
	char dataBuffer[CHUNK_SIZE];//32K
	std::string COMPRESSED_DATAStr;
	int C_STATUS;

	do{
		zstream->next_out = reinterpret_cast<Bytef*>(dataBuffer);
		zstream->avail_out = sizeof(dataBuffer);
		C_STATUS = deflate(zstream,Z_FINISH);
		switch(C_STATUS){
			case Z_BUF_ERROR:
				std::cout<<"Z_BUF_ERROR";
				return nullptr;
				//handle exception
			case Z_STREAM_ERROR:
				std::cout<<"Z_STREAM_ERROR";
				return nullptr;
				//handle exception
			case Z_MEM_ERROR:
				std::cout<<"Z_MEM_ERROR";
				return nullptr;
		}
		COMPRESSED_DATAStr.append(dataBuffer,sizeof(dataBuffer) - zstream->avail_out);
	}while(C_STATUS == Z_OK);
	deflateEnd(zstream);
	if(C_STATUS != Z_STREAM_END){
		//handle error
	}
	/*Cleanup*/
	delete zstream;
	return COMPRESSED_DATAStr;
}



std::string Compressor::DeflateF(int compressorWindowBits,int compressorMemoryLevel,int compressorMethod){
	if(this->filename == NULL || this->DEFLATE_ENABLE != 1){
		//throw exception
	}
	z_stream *zstream = new z_stream();
	std::ifstream inputFile(this->filename,ios::binary | ios::in);
	zstream->opaque = Z_NULL;
	zstream->zalloc = Z_NULL;
	zstream->zfree = Z_NULL;
	
	string dataExtract;
	inputFile>>dataExtract;
	string outfileName = (string) this->filename;
	outfileName = outfileName + ".gz";

	zstream->next_in = (Bytef*)dataExtract.data();
	zstream->avail_in = dataExtract.size();
	deflateInit2(zstream,this->levelDecoder(compressionLevel),Z_DEFLATED,compressorWindowBits,compressorMemoryLevel,compressorMethod);
	
	char dataBuffer[CHUNK_SIZE];
	string COMPRESSED_DATA;
	std::ofstream COMPRESSED_FILE(outfileName.c_str(),ios::out | ios::binary);
	int C_STATUS;

	do{
		zstream->next_out = reinterpret_cast<Bytef*>(dataBuffer);
		zstream->avail_out = sizeof(dataBuffer);

		C_STATUS = deflate(zstream,Z_FINISH);
		switch(C_STATUS){
			case Z_BUF_ERROR:
				return "Z_BUF_ERROR";
			case Z_STREAM_ERROR:
				return "Z_STREAM_ERROR";
			case Z_MEM_ERROR:
				return "Z_MEM_ERROR";
		}
		COMPRESSED_DATA.append(dataBuffer,sizeof(dataBuffer) - zstream->avail_out);
	}while(C_STATUS == Z_OK);
	deflateEnd(zstream);
	if(C_STATUS != Z_STREAM_END){
		//throw exception
	}
	COMPRESSED_FILE<<COMPRESSED_DATA;

	/*Cleanup*/
	delete zstream;
	return COMPRESSED_DATA;
}
std::string Compressor::Inflate(char* compressed,size_t len){
    z_stream *zs = new z_stream();
	zs->next_in = Z_NULL;
	zs->avail_in = 0;
	zs->opaque = Z_NULL;
	zs->zalloc = Z_NULL;
	zs->zfree = Z_NULL;
	inflateInit(zs);
	char outBuff[32768];
	string out;
	zs->next_in = reinterpret_cast<Bytef*>(compressed);
	zs->avail_in = len;
	int ret;
	do{
		zs->next_out = reinterpret_cast<Bytef*>(outBuff);
		zs->avail_out = sizeof(outBuff);
		ret = inflate(zs,Z_NO_FLUSH);
		out.append(outBuff,sizeof(outBuff) - zs->avail_out);
	}while(ret == Z_OK && zs->avail_out == 0);
	/*if(ret != Z_STREAM_END){
		cout<<"Error: "<<ret<<endl;
	}*/
	decompressedSize = out.size();
	return out;
 }

int Compressor::levelDecoder(int compressionLevel){
	if(DEFLATE_ENABLE){
		switch(compressionLevel){
			case RATIO_MAX:
				return DEFLATE_BEST_RATIO;
			case RATIO_MOD:
				return DEFLATE_BALANCE;
			case RATIO_MIN:
				return DEFLATE_MIN;
		}
	}
	return -1;
}
