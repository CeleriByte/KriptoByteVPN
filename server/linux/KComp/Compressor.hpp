#ifndef COMPRESSOR_H
#define COMPRESSOR_H

#include <iostream>
#include <zlib.h>

#define RATIO_MAX 0 //best compression ratio
#define RATIO_MOD 1 //balance between speed and level of compression
#define RATIO_MIN 2 //minimal compression

//DEFLATE specs
#define DEFLATE_BEST_RATIO 9
#define DEFLATE_BALANCE 6
#define DEFLATE_MIN 1
#define CHUNK_SIZE 32768 

#define ALGORITHM_DEFLATE 0

#define MEMORY_LARGEST 9
#define MEMORY_AVERAGE 5
#define MEMORY_LOWEST 1

#define HUFFMAN_ONLY Z_HUFFMAN_ONLY
#define METHOD_DEFAULT Z_DEFAULT_STRATEGY
#define HUFFMAN_OVER_STRMATCH Z_FILTERED
#define PNG_SPECIALIZE Z_RLE



class Compressor{
	public:
		Compressor(char* subjectData,char* filename,int compressionLevel=RATIO_MAX,int algorithm = ALGORITHM_DEFLATE);
		Compressor();
		virtual ~Compressor();
		std::string Deflate(int compressorWindowBits = 15,int compressorMemoryLevel = MEMORY_LARGEST,int compressorMethod = METHOD_DEFAULT);
		std::string DeflateF(int compressorWindowBits = 15,int compressorMemoryLevel = MEMORY_LARGEST,int compressorMethod = METHOD_DEFAULT);
		std::string Inflate(char* compressed,size_t);
		int errorCheck(char *subjectData,char *filename);
		//void log();
		int levelDecoder(int compressionLevel);
		int decompressedSize;	
		int buffSize;
	private:
		/*Flag to check whether Deflate algorithm
		 * is being used  */
		int DEFLATE_ENABLE;
		int compressionLevel;
		int compressorWindowBits;
		int compressorMemoryLevel;
		int compressorMethod;
		char *subjectData;
		char *filename;


};
#endif
