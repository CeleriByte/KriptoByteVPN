#ifndef STORAGE_H
#define STORAGE_H

#include <memory.h>
#include <iostream>
#include <stdlib.h>
#include <cppconn/driver.h>
#include <cppconn/statement.h>
#include <cppconn/resultset.h>
#include <cppconn/prepared_statement.h>
#include <cstdint>
#include <vector>
#include <string>

#define MAX_API_CALLS 0

class Storage{
	public:
		Storage();
		~Storage();
		bool Authenticate(std::string,std::string);
		bool Register(std::string,std::string);
	private:
		sql::SQLString serverU,serverP,serverA;
		sql::Driver *driver;
		sql::Connection *connection;
		sql::Statement *statement;
		sql::PreparedStatement *preparedStmt;
};

#endif
