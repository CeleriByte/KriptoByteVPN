package com.kriptobyte;

import javax.net.ssl.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Main {
    static {
        System.loadLibrary("vpnstorage");
        System.loadLibrary("READ");

    }
    public static final int STORAGE_POOL = 150;
    public static final String MAX_IP = "10.15.255.254";
    public static final String ROOT = "toor";
    public static final StoragePoolAbstraction storagePoolAbstraction = new StoragePoolAbstraction();
    public static final PrivateIpAssigner privateIpAssigner = new PrivateIpAssigner(MAX_IP);
    public static final TunnelManager tunnelManager = new TunnelManager(ROOT);
    public static final ProcessManager processManager = new ProcessManager();
    public static void main(String[] args) {
        String KeyStoreName = "CEHSDLKeyStore";
        char[] KeyStorePass = "rapidbyte".toCharArray();
        char[] CertificatePass = "rapidbyte".toCharArray();
        ConfigFileRead configFileRead = new ConfigFileRead(args[1]);
        int corePoolSize = configFileRead.getCorePoolSize();
        int maximumPoolSize = configFileRead.getMaximumPoolSize();
        long keepAliveTime = configFileRead.getKeepAliveTime();

        ExecutorService executorService = new ThreadPoolExecutor(corePoolSize,maximumPoolSize,keepAliveTime, TimeUnit.MILLISECONDS,new LinkedBlockingQueue<Runnable>());
        try{
            KeyStore keyStore = KeyStore.getInstance("JKS");
            keyStore.load(new FileInputStream(KeyStoreName),KeyStorePass);
            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
            keyManagerFactory.init(keyStore,CertificatePass);

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(keyManagerFactory.getKeyManagers(),null,new SecureRandom());
            SSLServerSocketFactory sslServerSocketFactory = sslContext.getServerSocketFactory();
            SSLServerSocket sslServerSocket = (SSLServerSocket) sslServerSocketFactory.createServerSocket(Integer.parseInt(args[0]));

            System.out.println("[*] Server Initialized And Running");
            while(true) {
                SSLSocket sslSocket = (SSLSocket) sslServerSocket.accept();
                executorService.execute(new ConnectionHandler(sslSocket));
            }

        }catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

    }
}
