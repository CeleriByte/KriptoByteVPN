package com.kriptobyte;

import javax.net.ssl.SSLSocket;
import javax.xml.bind.DatatypeConverter;
import java.io.*;

/**
 * Created by ziscky on 6/16/15.
 */
public class ConnectionHandler implements Runnable{
    private SSLSocket sslSocket;
    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;
    private RequestCodes requestCodes;
    private StorageBridge storageBridge;
    public ConnectionHandler(SSLSocket clientSocket) throws IOException {
        this.sslSocket = clientSocket;
        dataInputStream = new DataInputStream(clientSocket.getInputStream());
        dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());
        requestCodes = new RequestCodes();
    }

    @Override
    public void run() {
        System.out.println("[*]Connected: "+sslSocket.getInetAddress());
        try {
            String initializationFlag = dataInputStream.readUTF();
            if(initializationFlag.equals(requestCodes.AUTH_REQUEST)){
                dataOutputStream.writeUTF(requestCodes.CONTINUE);
                dataOutputStream.flush();

                String authDetails = dataInputStream.readUTF();
             System.out.println(authDetails);
                String[] splitDetails = checkIntegrity(authDetails);
                if(splitDetails == null){
                    dataOutputStream.writeUTF(requestCodes.AUTH_FAIL);
                    dataOutputStream.flush();
                    return;
                }
                synchronized (Main.storagePoolAbstraction){
                    storageBridge = Main.storagePoolAbstraction.StorageConnectionPool.borrowObject();
                }
                boolean authenticated = storageBridge.Authenticate(splitDetails[0],splitDetails[1]);
                if(!authenticated) {
                    dataOutputStream.writeUTF(requestCodes.AUTH_FAIL);
                    dataOutputStream.flush();
                    return;
                }
                dataOutputStream.writeUTF(requestCodes.AUTHENTICATED);
                dataOutputStream.flush();

            }else if(initializationFlag.equals(requestCodes.ENCRYPTION_KEY_REQUEST)){
             System.out.println(initializationFlag);
                dataOutputStream.writeUTF(requestCodes.CONTINUE);
                dataOutputStream.flush();

                String request = dataInputStream.readUTF();

                if(!request.equals(requestCodes.KEY_REQUEST)){}
                VpnInstance vpnInstance =  vpnSetup();
                if(vpnInstance.initializationVectorExists()){
                    dataOutputStream.writeUTF(requestCodes.KEY_GOOD);
                    dataOutputStream.flush();
                }else{
                    dataOutputStream.writeUTF(requestCodes.DATA_ERR);
                    dataOutputStream.flush();
                }
                String publicKey = readKey(vpnInstance);
                dataOutputStream.writeUTF(publicKey);
                dataOutputStream.flush();


                String privRequest = dataInputStream.readUTF();
                if(!privRequest.equals(requestCodes.IV_REQUEST)){}
                if(vpnInstance.keyFileExists()){
                    dataOutputStream.writeUTF(requestCodes.IV_GOOD);
                    dataOutputStream.flush();
                }else{
                    dataOutputStream.writeUTF(requestCodes.DATA_ERR);
                    dataOutputStream.flush();
                }
                String privateKey = readIV(vpnInstance);
                dataOutputStream.writeUTF(privateKey);
                dataOutputStream.flush();

                String confParams = formatParameters(vpnInstance.getDstAddrIp(),vpnInstance.getPort());
                dataOutputStream.writeUTF(confParams);
                dataOutputStream.flush();

            }
        } catch (IOException e) {
            endConnection();
            e.printStackTrace();
        } catch (Exception e) {
            endConnection();
            e.printStackTrace();
        }
        endConnection();

    }
//ISO-8859-1
    private String readIV(VpnInstance vpnInstance) throws IOException {
        File file = new File(vpnInstance.getProcId() + "/ivfile");
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line;
        String ivStr = "";
        while ((line = bufferedReader.readLine()) != null) {
            ivStr += line;
        }
        bufferedReader.close();
        return ivStr;
    }

    private String readKey(VpnInstance vpnInstance) throws IOException {
        File file = new File(vpnInstance.getProcId() + "/keyfile");
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line;
        String keyStr = "";
        while ((line = bufferedReader.readLine()) != null) {
            keyStr += line;
        }
        bufferedReader.close();
        return keyStr;
    }
    private String formatParameters(String privateIp,String port){
        return privateIp+":"+port+";";
    }
    private void endConnection(){
        try {
            dataOutputStream.close();
            dataInputStream.close();
            sslSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private VpnInstance vpnSetup() throws IOException, InterruptedException {//TODO
        VpnInstance vpnInstance;
        synchronized (Main.processManager){
            vpnInstance = Main.processManager.getVpnInstance();
        }
        return vpnInstance;
    }
    private String[] checkIntegrity(String authDetails){
        String[] details = authDetails.split(":");
        if(details.length > 2){
            return null;
        }
        if(!details[1].endsWith(";")){
            return null;
        }
        String email = details[0];
        String pass = details[1].replace(";","");
        return new String[]{email,pass};
    }
}
