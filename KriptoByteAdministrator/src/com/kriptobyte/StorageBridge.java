package com.kriptobyte;

import com.kriptobyte.Storage.Storage;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.UnsupportedEncodingException;

/**
 * Created by ziscky on 6/16/15.
 */
/*Bridge To Native StorageEngine*/
public class StorageBridge {
    Storage storageInstance;
    public StorageBridge(){
        storageInstance = new Storage();
    }
    public boolean Authenticate(String email,String pass){
        return storageInstance.Authenticate(email,sha1(pass));
    }
    private String sha1(String str){
        String sha = DigestUtils.sha1Hex(str.getBytes());

    System.out.println(sha);
        return sha;
    }
    public void reset(){}//Later Use
}
