package com.kriptobyte;


import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by ziscky on 6/16/15.
 */
public class ProcessManager {
    private TreeSet<VpnInstance> activeVpnInstances;
    private TreeSet<VpnInstance> inactiveVpnInstances;
    private TreeSet<String> usedIds;
    private Hashtable<VpnInstance,Integer> potentialRecycles;//Stores procId and no. of inactive hits,Max: 5
    private SecureRandom secureRandom;
    private int LOWER_LIMIT = 7500;
    private int UPPER_LIMIT = 65000;
    public ProcessManager() {
        activeVpnInstances = new TreeSet<>();
        inactiveVpnInstances = new TreeSet<>();
        secureRandom = new SecureRandom();
        potentialRecycles = new Hashtable<>();
        usedIds = new TreeSet<>();
        ScheduledExecutorService recycler = Executors.newScheduledThreadPool(1);
        ScheduledFuture scheduledFuture = recycler.scheduleWithFixedDelay(()->{
            synchronized (potentialRecycles) {
                Iterator<VpnInstance> iterator = activeVpnInstances.iterator();
                try {
                    while (iterator.hasNext()) {
                        VpnInstance vpnInstanceGc = iterator.next();
                /*Recycle Dead Instances*/
                        if (!vpnInstanceGc.isAlive()) {
                            InstanceCorrection(vpnInstanceGc);
                        }
                    /*Recycle Idle Instances*/
                        if (vpnInstanceGc.currentState() == 102) {
                            if (potentialRecycles.containsKey(vpnInstanceGc)) {
                                int hits = potentialRecycles.get(vpnInstanceGc);
                                potentialRecycles.put(vpnInstanceGc, hits + 1);
                            } else {
                                potentialRecycles.put(vpnInstanceGc, 1);
                            }
                        } else if (vpnInstanceGc.currentState() == 101) {//Decrease Hit Counter for Instances that resumed normal functions
                            potentialRecycles.put(vpnInstanceGc, potentialRecycles.get(vpnInstanceGc) - 1);
                        }
                    }
                /*Recycle VpnInstance Objects that have remained inactive through 5 Maintenance ops*/
                    Iterator hashTableIterator = potentialRecycles.entrySet().iterator();
                    while (hashTableIterator.hasNext()) {
                        Map.Entry pair = (Map.Entry) hashTableIterator.next();
                        if ((int) pair.getValue() >= 5) {
                            potentialRecycles.remove((VpnInstance) pair.getKey());
                            InstanceCorrection((VpnInstance) pair.getKey());
                        }
                    }
                } catch (Exception e) {
                    //ignore
                }
            }
        },60,60, TimeUnit.MINUTES);
    }
    public VpnInstance getVpnInstance() throws IOException, InterruptedException {
        /*Check For Inactive VPN instance*/
        if(inactiveVpnInstances.size() > 0){
            Iterator<VpnInstance> vpnInstanceIterator = inactiveVpnInstances.iterator();
            while(vpnInstanceIterator.hasNext()){
                VpnInstance probableInstance = vpnInstanceIterator.next();
                if(probableInstance.isAlive()){
                    return probableInstance;
                }else{
                    InstanceCorrection(probableInstance);
                }
            }
        }
        /*Get process Id*/
        String processId;
        while (usedIds.contains(processId = generateId())){}//Make Sure procId is unique
        usedIds.add(processId);
        /*Create Process Working Directory*/
        if(!new File(processId).mkdir()){
            throw new IOException("Could Not create process directory");
        }
        this.notifyAll();
        /*Request For Process IpAddress*/
        String privateIp,dstAddrIp;
        synchronized (Main.privateIpAssigner){
            privateIp = Main.privateIpAssigner.Assign();
            dstAddrIp = Main.privateIpAssigner.Assign();
        }
    System.out.println(privateIp+":"+dstAddrIp);
        /*Request Tunnel Interface*/
        boolean tunnelOnline;
        String tunnelDescriptor;
        synchronized (Main.tunnelManager){
            tunnelDescriptor = Main.tunnelManager.getRawTunnelDescriptor();
            if(!Main.tunnelManager.createTunnel(tunnelDescriptor)){
        System.out.println("SERVER ERROR");
            }
            tunnelOnline = Main.tunnelManager.takeTunnelOnline(tunnelDescriptor,privateIp,dstAddrIp);
        }
        if(tunnelDescriptor == null){//System has hit capacity
            /*If a new VpnInstance is disconnected give it out*/
            if(inactiveVpnInstances.size() > 0){
                Iterator<VpnInstance> secondaryVpnInstanceIter = inactiveVpnInstances.iterator();
                while (secondaryVpnInstanceIter.hasNext()){
                    VpnInstance secondaryVpnInstance = secondaryVpnInstanceIter.next();
                    if(secondaryVpnInstance.isAlive()){
                        return secondaryVpnInstance;
                    }else {
                        tunnelDescriptor = secondaryVpnInstance.getTunnelDescriptor();
                        InstanceCorrection(secondaryVpnInstance,1);
                        break;
                    }
                }

            }
        }
        /*Get Server Port*/
        int port = secureRandom.nextInt((UPPER_LIMIT-LOWER_LIMIT)+1)+LOWER_LIMIT;
        VpnInstance vpnInstance = new VpnInstance(tunnelDescriptor,privateIp,dstAddrIp,Integer.toString(port),processId);
        vpnInstance.Execute();
        activeVpnInstances.add(vpnInstance);
        return vpnInstance;
    }
    private String generateId(){
        return new BigInteger(128,secureRandom).toString(32);
    }
    private void InstanceCorrection(VpnInstance vpnInstance) throws IOException, InterruptedException {
        int dyingWish = vpnInstance.currentState();
        String errorStr = "";
        if(dyingWish == 404){
            errorStr += vpnInstance.getErrorString();
        }
        String procId = vpnInstance.getProcId();
        String tunnelDescriptor = vpnInstance.getTunnelDescriptor();
        String privateIp = vpnInstance.getPrivateIp();
        String dstAddrIp = vpnInstance.getDstAddrIp();
        String port = vpnInstance.getPort();
        vpnInstance.terminate();
        VpnInstance recycledVpnInstance = new VpnInstance(tunnelDescriptor,privateIp,dstAddrIp,port,procId);
        synchronized (activeVpnInstances){
        activeVpnInstances.add(recycledVpnInstance);}
    }
    private void InstanceCorrection(VpnInstance vpnInstance,int recycle) throws IOException {
        int dyingWish = vpnInstance.currentState();
        String errorStr = "";
        if(dyingWish == 404){
            errorStr += vpnInstance.getErrorString();
        }
        String procId = vpnInstance.getProcId();
        String tunnelDescriptor = vpnInstance.getTunnelDescriptor();
        String privateIp = vpnInstance.getPrivateIp();
        String dstAddrIp = vpnInstance.getDstAddrIp();
        String port = vpnInstance.getPort();
        /*Return All Borrowed Items and destroy the Instance*/
        synchronized (Main.tunnelManager){
            Main.tunnelManager.returnDescriptor(tunnelDescriptor);
        }
        synchronized (Main.privateIpAssigner){
            Main.privateIpAssigner.returnIps(privateIp,dstAddrIp);
        }
        synchronized (usedIds) {
            usedIds.remove(procId);
        }
        synchronized (activeVpnInstances){
        activeVpnInstances.remove(vpnInstance);}
        FileUtils.deleteDirectory(new File(procId));//Delete Process Directory And Contents
    }
}
