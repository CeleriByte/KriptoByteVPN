package com.kriptobyte;

import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ziscky on 6/16/15.
 */
public class VpnInstance implements Comparable{
    private String tunnelDescriptor;
    private String privateIp;
    private String dstAddrIp;
    private String port;
    private String procId;
    private Process process;
    private BufferedReader bufferedReader;
    private String errorString;
    private long creationTime;



    public VpnInstance(String tunnelDescriptor, String privateIp, String dstAddrIp, String port, String procId) throws IOException, InterruptedException {
        this.tunnelDescriptor = tunnelDescriptor;
        this.privateIp = privateIp;
        this.dstAddrIp = dstAddrIp;
        this.port = port;
        this.procId = procId;
        creationTime = System.currentTimeMillis();
        createEncryptionKeys();
    }
    public void Execute() throws IOException {
        FileUtils.copyFile(new File("KriptoByteVPNServer"),new File(procId + "/KriptoByteVPNServer"));
        FileUtils.copyFile(new File("Scripts/startServer.sh"),new File(procId+"/startServer.sh"));
        ProcessBuilder processManager = new ProcessBuilder("/bin/bash","./startServer.sh "+tunnelDescriptor+" "+port+" "+"test");;
        processManager.directory(new File(procId));
        process = processManager.start();
        bufferedReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
        System.out.println(currentState());
    }
    public int currentState() throws IOException {
        if(!isAlive()){
            return 504;//Process Exited
        }
        String recentOutput = "";
        String line;
        while((line = bufferedReader.readLine())!=null){
            recentOutput = line;
        }
        if(recentOutput.contains("busy")){
            errorString = recentOutput;
            return 404;//Process Stopped
        }
        if(recentOutput.contains("broken")){
            return 102;//Ok But Idle
        }
        return 101;//Ok
    }
    public void createEncryptionKeys() throws IOException, InterruptedException {
        FileUtils.copyFile(new File("Scripts/Generate.sh"),new File(procId+"/Generate.sh"));
        FileUtils.copyFile(new File("Scripts/ivfile"),new File(procId+"/ivfile"));
        FileUtils.copyFile(new File("Scripts/keyfile"),new File(procId+"/keyfile"));
        FileUtils.copyFile(new File("AESKeyGen"),new File(procId+"/AESKeyGen"));
        ProcessBuilder processBuilder = new ProcessBuilder("/bin/bash","./Generate.sh");

        processBuilder.directory(new File(procId));
        Process process = processBuilder.start();
        process.waitFor();
    }
    public boolean initializationVectorExists(){
        File file = new File(procId);
        ArrayList<String> ls = new ArrayList<>(Arrays.asList(file.list()));
        if(ls.contains("ivfile")){
            System.out.println("HAS IV");
        }
        return ls.contains("ivfile");
    }
    public boolean keyFileExists(){
        File file = new File(procId);
        ArrayList<String> ls = new ArrayList<>(Arrays.asList(file.list()));
        if(ls.contains("keyfile")){
            System.out.println("HAS KEY");
        }
        return ls.contains("keyfile");
    }
    public boolean isAlive(){
        return process.isAlive();
    }
    public boolean terminate(){
        process.destroy();
        return !isAlive();
    }
    public String getTunnelDescriptor() {
        return tunnelDescriptor;
    }

    public String getPrivateIp() {
        return privateIp;
    }

    public String getDstAddrIp() {
        return dstAddrIp;
    }

    public String getPort() {
        return port;
    }

    public String getProcId() {
        return procId;
    }

    public Process getProcess() {
        return process;
    }

    public BufferedReader getBufferedReader() {
        return bufferedReader;
    }

    public String getErrorString() {
        return errorString;
    }

    public long getCreationTime(){return creationTime;}
    @Override
    public int compareTo(Object o) {
        VpnInstance obj = (VpnInstance)o;
        if(this.creationTime < obj.getCreationTime()){
            return -1;
        }else if(this.creationTime > obj.getCreationTime()){
            return 1;
        }else{
            return 0;
        }
    }
}
