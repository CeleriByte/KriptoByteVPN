#include "Storage.hpp"
#include "mysql_connection.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <cppconn/driver.h>
#include <cppconn/statement.h>
#include <cppconn/resultset.h>

using namespace std;
using namespace sql;


Storage::Storage(){
		this->serverP = "";
		this->serverU = "root";
		this->driver = get_driver_instance();
		this->connection = this->driver->connect(this->serverA,this->serverU,this->serverP);	
		this->statement = this->connection->createStatement();
		this->statement->execute("USE kriptobyte");	
}
Storage::~Storage(){
	delete connection;
	delete statement;
	delete preparedStmt;
}
bool Storage::Authenticate(string email,string pass){
	string CHECK_QUERY = "SELECT * FROM kriptobyte_vpn_regular WHERE EMAIL = ? AND PASSWORD = ?";
	preparedStmt = this->connection->prepareStatement(CHECK_QUERY);
	preparedStmt->setString(1,email);
	preparedStmt->setString(2,pass);
	sql::ResultSet *resultSet = preparedStmt->executeQuery();
	if(!resultSet->next()){
		return false;
	}
	/*Cleanup*/
	delete resultSet;
	
	return true;
}
bool Storage::Register(string email,string pass){
	string CHECK_QUERY = "SELECT * FROM kriptobyte_vpn_regular WHERE EMAIL = ?";
	preparedStmt = this->connection->prepareStatement(CHECK_QUERY);
	preparedStmt->setString(1,email);
	sql::ResultSet *resultSet = preparedStmt->executeQuery();
	if(resultSet != nullptr){//Email Already Exists
		return false;
	}
	string INSERT_QUERY = "INSERT INTO kriptobyte_vpn_regular(EMAIL,PASSWORD) VALUES(?,?)";
	preparedStmt = this->connection->prepareStatement(INSERT_QUERY);
	preparedStmt->setString(1,email);
	preparedStmt->setString(2,pass);
	preparedStmt->execute();
	return 0;

}



