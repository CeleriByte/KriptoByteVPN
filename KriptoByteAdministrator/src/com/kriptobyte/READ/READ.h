#include <iostream>
#include <fstream>

class READ{
	public:
		READ();
		virtual ~READ();
		std::string readFile(std::string filename);
};
