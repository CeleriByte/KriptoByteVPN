#include <iostream>
#include <fstream>
#include <vector>
#include "READ.h"

using namespace std;
READ::READ(){}
READ::~READ(){}
std::string READ::readFile(std::string filename){
	std::ifstream file(filename.c_str(),ios::binary | ios::in);
	file.seekg(0,ios::end);
	int size = file.tellg();
	file.seekg(0,ios::beg);
	std::vector<char> bytes(size);
       	file.read(&bytes[0],size);
	string fileStr(bytes.begin(),bytes.end());
	return fileStr;
}
