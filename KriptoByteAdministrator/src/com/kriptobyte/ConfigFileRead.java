package com.kriptobyte;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

/**
 * Created by ziscky on 6/16/15.
 */
public class ConfigFileRead {
    private int corePoolSize;
    private int maximumPoolSize;
    private long keepAliveTime;
    private ArrayList<String> options;

    public ConfigFileRead(String configFileName) {
        options = new ArrayList<>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(configFileName));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if (line.startsWith("#")) {
                    System.out.println(line);
                    continue;
                }
                System.out.println(line);
                System.out.println(getOpt(line));
                options.add(getOpt(line));

            }
            bufferedReader.close();
        } catch (Exception e) {
        }
        corePoolSize = Integer.parseInt(options.get(0));
        maximumPoolSize = Integer.parseInt(options.get(1));
        keepAliveTime = Long.parseLong(options.get(2));
    }

    private String getOpt(String option) {
        return option.split(":")[1];
    }

    public int getCorePoolSize() {
        return corePoolSize;
    }


    public int getMaximumPoolSize() {
        return maximumPoolSize;
    }


    public long getKeepAliveTime() {
        return keepAliveTime;
    }
}
