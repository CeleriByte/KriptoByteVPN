package com.kriptobyte;

import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

/**
 * Created by ziscky on 6/16/15.
 */
public class StorageFactory extends BasePooledObjectFactory<StorageBridge> {
    @Override
    public StorageBridge create() throws Exception {
        return new StorageBridge();
    }

    @Override
    public PooledObject<StorageBridge> wrap(StorageBridge storageBridge) {
        return new DefaultPooledObject<StorageBridge>(storageBridge);
    }
    @Override
    public void passivateObject(PooledObject<StorageBridge> pooledObject) throws Exception {
        pooledObject.getObject().reset();
    }

}
