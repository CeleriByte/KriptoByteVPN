package com.kriptobyte;

import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

/**
 * Created by ziscky on 6/16/15.
 */
public class StoragePoolAbstraction {
    public final ObjectPool<StorageBridge> StorageConnectionPool = configurePool();
    private ObjectPool<StorageBridge> configurePool(){
        GenericObjectPoolConfig objectPoolConfig = new GenericObjectPoolConfig();
        objectPoolConfig.setMaxIdle(5);
        objectPoolConfig.setMaxTotal(Main.STORAGE_POOL);
        return new GenericObjectPool<StorageBridge>(new StorageFactory(),objectPoolConfig);

    }

}
