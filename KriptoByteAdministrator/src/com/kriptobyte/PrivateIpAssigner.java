package com.kriptobyte;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * Created by ziscky on 6/16/15.
 */
public class PrivateIpAssigner {
    private ArrayList<String> availableIpList;
    private ArrayList<String> usedIpList;
    private int ipSize;
    public PrivateIpAssigner(String maximumIp){
        availableIpList = new ArrayList<>();
        usedIpList = new ArrayList<>();
        String[] bits = maximumIp.replace(".","-").split("-");
        int subnet1 = Integer.parseInt(bits[1]);
        int subnet2 = Integer.parseInt(bits[2]);
        int subnet3 = Integer.parseInt(bits[3]);
        for (int i = 0; i <= subnet1; i++) {
            for (int j = 0; j <= subnet2 ; j++) {
                for (int k = 1; k <= subnet3 ; k++) {
                    availableIpList.add(10+"."+i+"."+j+"."+k);
                }
            }
        }
        ipSize = availableIpList.size();
    }
    public synchronized String Assign(){
        String Ip = null;
        Iterator<String> ipIter = availableIpList.iterator();
        while(ipIter.hasNext()){
            String probable = ipIter.next();
            if(!usedIpList.contains(probable)){
                Ip = probable;
                usedIpList.add(probable);
                break;
            }
        }
        if(Ip!=null){
            availableIpList.remove(Ip);
        }
        return Ip;
    }
    public synchronized void returnIps(String ip1,String ip2){
        usedIpList.add(ip1);
        usedIpList.add(ip2);
    }
    public int getSize(){
        return ipSize;
    }
}
