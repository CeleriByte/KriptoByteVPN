package com.kriptobyte;

/**
 * Created by ziscky on 6/16/15.
 */
public class RequestCodes {
    public String AUTH_REQUEST = "20";
    public String AUTHENTICATED = "202";
    public String AUTH_FAIL = "204";
    public String ENCRYPTION_KEY_REQUEST = "60";
    public String KEY_REQUEST = "601";
    public String KEY_GOOD = "602";
    public String IV_REQUEST = "701";
    public String IV_GOOD = "702";
    public String DATA_ERR = "604";
    public String CONTINUE = "101";
}
