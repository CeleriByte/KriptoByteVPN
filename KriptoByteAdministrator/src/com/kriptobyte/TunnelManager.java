package com.kriptobyte;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * Created by ziscky on 6/16/15.
 */
//Creates tunnel device
public class TunnelManager {
    private String rootPass;
    private int maximumTuns;
    private ArrayList<String> createdTuns;
    private ArrayList<String> usedTuns;
    public TunnelManager(String rootPass) {
        this.rootPass = rootPass;
        createdTuns = new ArrayList<>();
        usedTuns = new ArrayList<>();
        synchronized (Main.privateIpAssigner){
            maximumTuns+=Main.privateIpAssigner.getSize()/2;
        }
    }
    public String getRawTunnelDescriptor(){
        String tunnelDescriptor = null;
        for (int i = 0; i < maximumTuns; i++) {
            String descriptor = "tun"+i;
            if(!tunnelExists(descriptor) && !tunnelInUse(descriptor)){
                tunnelDescriptor = descriptor;
                maximumTuns-=1;
                usedTuns.add(descriptor);
                break;
            }
        }
        return tunnelDescriptor;
    }
    private boolean tunnelExists(String descriptor){
        return createdTuns.contains(descriptor);
    }
    private boolean tunnelInUse(String descriptor){
        return usedTuns.contains(descriptor);
    }
    public boolean createTunnel(String tunnelDescriptor) throws IOException {
        if(createdTuns.contains(tunnelDescriptor)){
            throw new IOException("Tunnel Already Created");
        }
        ProcessBuilder processBuilder = new ProcessBuilder("/bin/bash","-c","echo "+rootPass+"| sudo -S ./createTunnel.sh "+tunnelDescriptor);
        processBuilder.directory(new File("Scripts"));
        Process process = processBuilder.start();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
        String line;
        String output = "";
        while ((line=bufferedReader.readLine())!=null){
            output+=line;
        }
        if(output.contains("ioctl")){//Device Already Exists
            return false;
        }
        createdTuns.add(tunnelDescriptor);
        return true;
    }
    public boolean takeTunnelOnline(String tunnelDescriptor,String tunnelIp,String vpnClientIp) throws IOException {
        ProcessBuilder processBuilder = new ProcessBuilder("/bin/bash","-c","echo "+rootPass+"| sudo -S ./liveTunnel.sh "+tunnelDescriptor+" "+tunnelIp+" "+vpnClientIp);
        processBuilder.directory(new File("Scripts"));
        Process process = processBuilder.start();
        return true;
    }
    public void returnDescriptor(String tunnelDescriptor){
        usedTuns.remove(tunnelDescriptor);
        maximumTuns+=1;
    }
}
