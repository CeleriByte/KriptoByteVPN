package com.example.android.KriptoByteVPN;

import android.util.Base64;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;

/**
 * Created by ziscky on 6/14/15.
 */
public class AES {
    private Cipher cipher;
    byte[] key;
    byte[] iv;
    SecretKeySpec secureKey;
    IvParameterSpec ivParameterSpec;
    public AES(byte[] keyStr,byte[] ivStr) throws NoSuchPaddingException, NoSuchAlgorithmException, DecoderException {
        cipher = Cipher.getInstance("AES/CFB/NoPadding");
        key = Hex.decodeHex(new String(keyStr).toCharArray());
        iv = Hex.decodeHex(new String(ivStr).toCharArray());
        secureKey = new SecretKeySpec(key,"AES");
        ivParameterSpec = new IvParameterSpec(iv);
    }

    public byte[] Encrypt(byte[] packet) throws BadPaddingException, IllegalBlockSizeException, InvalidKeyException, DecoderException, InvalidAlgorithmParameterException {
        /*cipher.init(Cipher.ENCRYPT_MODE,secureKey,ivParameterSpec);
        byte[] encryptedPacket = cipher.doFinal(packet);
        return encryptedPacket;*/
        return packet;
    }
    public byte[] Decrypt(byte[] packet) throws InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, DecoderException, UnsupportedEncodingException {
        /*cipher.init(Cipher.DECRYPT_MODE,secureKey,ivParameterSpec);
        byte[] decryptedPacket = cipher.doFinal(packet);
        System.out.println(new String(decryptedPacket));
        return decryptedPacket;*/
        return packet;
    }
}
