package com.example.android.KriptoByteVPN;

import android.content.Context;
import android.content.Intent;
import android.net.VpnService;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;


import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

/**
 * Created by ziscky on 6/15/15.
 */
public class ParallelKeyExchange extends AsyncTask<String,Void,ArrayList<String>>{
    Context appCtx;
    TextView textView;
    ProgressBar progressBar;
    Button buttonStart,button;
    ArrayList<String> encryptionKeys;
    PreComm preComm;
    public ParallelKeyExchange(Context appCtx,ArrayList<String> encryptionKeys,TextView textView,ProgressBar progressBar,Button buttonStart,Button button) {
        this.appCtx = appCtx;
        this.textView = textView;
        this.progressBar = progressBar;
        this.encryptionKeys = encryptionKeys;
        this.button = button;
        this.buttonStart = buttonStart;

    }

    @Override
    protected ArrayList<String> doInBackground(String... params) {
        try {
            preComm = new PreComm();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(preComm != null){
            try {
                ArrayList<String> mEncryptionKeys = preComm.requestEncryptionKeys();
            System.out.println(mEncryptionKeys.size());
                return mEncryptionKeys;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<String> mEncryptionKeys) {
        this.encryptionKeys.add(mEncryptionKeys.get(0));
        this.encryptionKeys.add((mEncryptionKeys.get(1)));
        this.encryptionKeys.add(mEncryptionKeys.get(2));
        textView.setText("Connecting To Server...");
        progressBar.setVisibility(View.INVISIBLE);
        buttonStart.setVisibility(View.VISIBLE);
        button.setVisibility(View.INVISIBLE);
    }
}
