package com.example.android.KriptoByteVPN;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by ziscky on 6/15/15.
 */
public class ParallelAuthentication extends AsyncTask<String,Void,Boolean>{
    Context appCtx;
    ProgressBar progressBar;
    TextView textView;
    Button button;
    PreComm preComm;
    boolean exception;
    public ParallelAuthentication(Context appCtx,ProgressBar progressBar,TextView textView,Button button) {
        this.appCtx = appCtx;
        this.progressBar = progressBar;
        this.textView = textView;
        this.button = button;
    }

    @Override
    protected Boolean doInBackground(String... params) {
        if(textView.getText().charAt(0) == 'F' || textView.getText().charAt(1) == 'W'){
            textView.setVisibility(View.INVISIBLE);
        }
        button.setClickable(false);
        try {
            preComm = new PreComm();
        } catch (NoSuchAlgorithmException e) {
            exception = true;
            e.printStackTrace();
        } catch (KeyManagementException e) {
            exception = true;
            e.printStackTrace();
        } catch (IOException e) {
            exception = true;
            e.printStackTrace();
        }
        boolean authenticated = false;
        try {
            if(preComm == null){
                throw new IOException("Couldn't connect to server");
            }
            authenticated = preComm.transmitAuthMessage(params[0],params[1]);
        } catch (IOException e) {
            exception = true;
            e.printStackTrace();
        }
        return authenticated;
    }
    @Override
    protected void onPostExecute(Boolean authentication) {
        progressBar.setVisibility(View.INVISIBLE);
        if(exception){
            button.setClickable(true);
            textView.setText("Failed to Connect,Check your Internet connection");
            Toast.makeText(appCtx,"Failed to Connect,Check your Internet connection",Toast.LENGTH_SHORT).show();
            return;
        }
        if(!authentication){
            button.setClickable(true);
            textView.setText("Wrong Email/Password Combination");
            Toast.makeText(appCtx,"Wrong Email/Password Combination",Toast.LENGTH_SHORT).show();
        }else{
            textView.setVisibility(View.INVISIBLE);
            Intent intent = new Intent(appCtx,VpnConnect.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            appCtx.startActivity(intent);

        }
    }
}
