package com.example.android.KriptoByteVPN;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.VpnService;
import android.os.Handler;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;
import com.example.android.toyvpn.R;
import org.apache.commons.codec.DecoderException;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class KriptoByteVpnService extends VpnService implements Handler.Callback, Runnable {
    private static final String TAG = "KriptoByteVpnService";
    private String mServerAddress = "192.168.0.29";
    private String mServerPort;
    private String privateIp;
    private byte[] mSharedSecret = "test".getBytes();;
    private PendingIntent mConfigureIntent;
    private AES aesHandler;
    private Handler mHandler;
    private Thread mThread;
    private String publicKeyStr;
    private String privateKeyStr;
    private ParcelFileDescriptor mInterface;
    private String mParameters;
    private CustomDeflate customDeflate;
    private int totalSaved;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // The handler is only used to show messages.
        if (mHandler == null) {
            mHandler = new Handler(this);
        }

        // Stop the previous session by interrupting the thread.
        if (mThread != null) {
            mThread.interrupt();
        }
        customDeflate = new CustomDeflate();
        // Extract information from the intent
        String prefix = getPackageName();
        publicKeyStr = intent.getStringExtra(prefix + ".KEY");
        privateKeyStr = intent.getStringExtra(prefix + ".IV");
        mServerPort = intent.getStringExtra(prefix + ".PORT");
        privateIp = intent.getStringExtra(prefix + ".ADDRESS");
        try {
            System.out.println(publicKeyStr);
            aesHandler = new AES(publicKeyStr.getBytes(),privateKeyStr.getBytes());
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (DecoderException e) {
            e.printStackTrace();
        }

        totalSaved=0;
        // Start a new session by creating a new thread.
        mThread = new Thread(this, "KriptoByteVpnThread");
        mThread.start();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        if (mThread != null) {
            mThread.interrupt();
        }
    }

    @Override
    public boolean handleMessage(Message message) {
        if (message != null) {
            Toast.makeText(this, message.what, Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    @Override
    public synchronized void run() {
        try {
            Log.i(TAG, "Starting");

            InetSocketAddress server = new InetSocketAddress(mServerAddress, Integer.parseInt(mServerPort));
            for (int attempt = 0; attempt < 10; ++attempt) {
                mHandler.sendEmptyMessage(R.string.connecting);
                // Reset the counter if we were connected.
                if (run(server)) {
                    attempt = 0;
                }
                // Sleep for a while. This also checks if we got interrupted.
                Thread.sleep(3000);
            }
            Log.i(TAG, "Giving up");
        } catch (Exception e) {
            Log.e(TAG, "Got " + e.toString());
        } finally {
            try {
                mInterface.close();
            } catch (Exception e) {
                // ignore
            }
            mInterface = null;
            mParameters = null;

            mHandler.sendEmptyMessage(R.string.disconnected);
            Log.i(TAG, "Exiting");
        }
    }

    private boolean run(InetSocketAddress server) throws Exception {
        DatagramChannel tunnel = null;
        boolean connected = false;
        try {
            // Create a DatagramChannel as the VPN tunnel.
            tunnel = DatagramChannel.open();

            // Protect the tunnel
            if (!protect(tunnel.socket())) {
                throw new IllegalStateException("Cannot protect the tunnel");
            }

            tunnel.connect(server);
            tunnel.configureBlocking(false);
            handshake(tunnel);
           connected = true;
            mHandler.sendEmptyMessage(R.string.connected);

            FileInputStream in = new FileInputStream(mInterface.getFileDescriptor());
            FileOutputStream out = new FileOutputStream(mInterface.getFileDescriptor());

            ByteBuffer packet = ByteBuffer.allocate(32767);
            ByteBuffer compactPacket;
            int ioSwitch = 0;

            // We keep forwarding packets till something goes wrong.
            while (true) {
                boolean idle = true;
                int length = in.read(packet.array());
                if (length > 0) {
                    packet.limit(length);
                    packet.position(0);
                    compactPacket = ByteBuffer.allocate(length);
                    for (int i = 0; i < length; i++) {
                        compactPacket.put(i,packet.get(i));
                    }
                    ByteBuffer compressedPacket = customDeflate.Deflate(compactPacket,length);
                    /*If compression yields acceptable results add flag to packet header*/
                    if(compactPacket.capacity() > compressedPacket.limit()){
                        System.out.println("Actual: "+packet.limit() + "<-::::->PostC: "+compressedPacket.limit());
                        ByteBuffer packetWithCompressionFlag = ByteBuffer.allocate(compressedPacket.limit()+1);
                        packetWithCompressionFlag.put(0,(byte)'c');
                        packetWithCompressionFlag.position(1);
                        packetWithCompressionFlag.put(compressedPacket.array());
                        packetWithCompressionFlag.position(0);
                        byte[] encryptedPacketWCF = aesHandler.Encrypt(packetWithCompressionFlag.array());
                        tunnel.write(ByteBuffer.wrap(encryptedPacketWCF));
                        totalSaved+=(packet.limit()-compressedPacket.limit());
                    }else {
                        compactPacket.position(0);
                        byte[] encryptedPacket = aesHandler.Encrypt(compactPacket.array());
                        int wr = tunnel.write(ByteBuffer.wrap(encryptedPacket));
                        System.out.println("Sent: "+wr + " Read: "+length);
                    }
                    packet.clear();
                    compactPacket.clear();
                    idle = false;
                    //Switch to sending
                    if (ioSwitch < 1) {
                        ioSwitch = 1;
                    }
                }

                // Read the incoming packet from the tunnel.
                length = tunnel.read(packet);
                if (length > 0) {
                    // Ignore control messages, which start with zero
                    // Or packets with the decompress flag.
                    packet.limit(length);
                    packet.position(0);
                    compactPacket = ByteBuffer.allocate(length);
                    for (int i = 0; i < length; i++) {
                        compactPacket.put(i,packet.get(i));
                    }
                    ByteBuffer decryptedPacketBuff;
                    if (packet.get(0) != 0 && packet.get(0) != 'd') {
                        byte[] decryptedPacket = aesHandler.Decrypt(compactPacket.array());
                        System.out.println("Decrypted Packet Length: "+length);
                        decryptedPacketBuff = ByteBuffer.wrap(decryptedPacket);
                        out.write(decryptedPacketBuff.array());
                    }else if(packet.get(0) == 'd'){//Compressed Packet
                        System.out.println("[*] Decompressing Packet");
                        compactPacket.limit(length);
                        compactPacket.position(0);
                        byte[] decompressedPacket = customDeflate.Inflate(removeHeader(compactPacket));
                        byte[] decryptedPacket = aesHandler.Decrypt(decodePacket(decompressedPacket));
                        out.write(decryptedPacket,0,decryptedPacket.length);
                        totalSaved+=(decryptedPacket.length - length);
                    }
                    packet.clear();
                    compactPacket.clear();
                    // There might be more incoming packets.
                    idle = false;
                    if (ioSwitch > 0) {
                        ioSwitch = 0;
                    }
                }

                if (idle) {
                    Thread.sleep(100);
                    ioSwitch += (ioSwitch > 0) ? 100 : -100;
                    if (ioSwitch < -15000) {
                        // Send empty control messages.
                        packet.put((byte) 0).limit(1);
                        ByteBuffer compact = ByteBuffer.allocate(1);
                        compact.put(0,packet.get(0));
                        for (int i = 0; i < 3; ++i) {
                            tunnel.write(ByteBuffer.wrap(aesHandler.Encrypt(compact.array())));
                        }
                        packet.clear();
                        ioSwitch = 1;
                    }
                    if (ioSwitch > 20000) {
                        throw new IllegalStateException("Timed out");
                    }
                }
            }
        } catch (InterruptedException e) {
            System.out.println("TotalSavedBytes: "+totalSaved);
            Toast.makeText(getApplicationContext(),"TotalSavedBytes: "+totalSaved,Toast.LENGTH_LONG).show();
            throw e;
        } catch (Exception e) {
            Log.e(TAG, "Got " + e.toString());
            e.printStackTrace();
        } finally {
            try {
                tunnel.close();
            } catch (Exception e) {
                // ignore
            }
        }
        return connected;
    }
    private byte[] removeHeader(ByteBuffer packet){
        byte[] noHeader = new byte[packet.capacity() -1];
        packet.limit(packet.capacity());

        packet.position(1);
        for (int i = 0; i < packet.capacity()-1; i++) {
           noHeader[i] = packet.get();
        }
        return noHeader;
    }
    private byte[] decodePacket(byte[] packet){
        System.out.println(new String(packet));
        return android.util.Base64.decode(packet, Base64.NO_WRAP);

    }
    private void handshake(DatagramChannel tunnel) throws Exception {
        ByteBuffer packet = ByteBuffer.allocate(1024);
        //Control Message Starts With 0
        packet.put((byte) 0).put(mSharedSecret).flip();

        for (int i = 0; i < 3; ++i) {
            packet.position(0);
            tunnel.write(packet);
        }
        packet.clear();
        for (int i = 0; i < 50; ++i) {
            Thread.sleep(100);
            int length = tunnel.read(packet);
            if (length > 0) {
                configure();
                return;
            }
        }
        throw new IllegalStateException("Timed out");
    }

    private void configure() throws Exception {
        // If the old interface has exactly the same parameters, use it!
        Builder builder = new Builder();
        try {
            mInterface.close();
        } catch (Exception e) {
            // ignore
        }
        mInterface = builder.setSession(mServerAddress).addAddress("10.0.0.2",32).addDnsServer("8.8.8.8").setMtu(1400).addRoute("0.0.0.0",0).establish();

    }
}