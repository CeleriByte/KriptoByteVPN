package com.example.android.KriptoByteVPN;

import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

/**
 * Created by ziscky on 6/13/15.
 */
public class CustomDeflate {
    public ByteBuffer Deflate(ByteBuffer packetData,int actualLen){
        byte[] packetBytes = packetData.array();
        byte[] compressedPacketBytes = new byte[1024];
        byte[] trimmmedPacketBuffer = new byte[actualLen];
        System.arraycopy(packetBytes,0,trimmmedPacketBuffer,0,actualLen);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(packetData.limit());
        Deflater deflater = new Deflater();
        deflater.setLevel(Deflater.BEST_COMPRESSION);
        deflater.setStrategy(Deflater.DEFAULT_STRATEGY);

        deflater.setInput(trimmmedPacketBuffer);
        deflater.finish();
        while(!deflater.finished()) {
            int compressedLen = deflater.deflate(compressedPacketBytes);
            outputStream.write(compressedPacketBytes,0,compressedLen);
        }
        try {
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] limitedCPB = outputStream.toByteArray();
        ByteBuffer compressedPacket = ByteBuffer.wrap(limitedCPB);
        return compressedPacket;
    }
    public byte[] base64(byte[] packet){
        return Base64.encode(packet,Base64.NO_PADDING | Base64.NO_WRAP);
    }
    public byte[] Inflate(byte[] compressedPacket){
        Inflater inflater = new Inflater();
        byte[] finalContents = null;
        int numberOfBytes = compressedPacket.length;
        inflater.setInput(compressedPacket,0,numberOfBytes);

        int numCompressedBytes = 0;
        List<Byte> decompressedBytes = new ArrayList<Byte>();
        try{
            while (!inflater.needsInput()){
                byte[] decompressedByteBuffer = new byte[1024];
                int numCompressed = inflater.inflate(decompressedByteBuffer);
                numCompressedBytes+=numCompressed;
                for (int i = 0; i < numCompressed; i++) {
                    decompressedBytes.add(decompressedByteBuffer[i]);
                }
            }
            finalContents = new byte[decompressedBytes.size()];
            for (int i = 0; i <finalContents.length ; i++) {
                finalContents[i] = decompressedBytes.get(i);
            }
        }catch (DataFormatException e) {
            e.printStackTrace();
        }
        inflater.end();
        return finalContents;
    }
}
