package com.example.android.KriptoByteVPN;

import javax.net.ssl.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

/**
 * Created by ziscky on 6/15/15.
 */
public class PreComm {
    private SSLContext sslContext;
    private SSLSocketFactory sslSocketFactory;
    private SSLSocket sslSocket;
    private String SERVER_ADDR = "192.168.0.33";
    private String SERVER_PORT = "7200";
    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;
    private boolean loggedIn;
    public PreComm() throws NoSuchAlgorithmException, KeyManagementException, IOException {
        TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {  }
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {  }
                }
        };
        sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null,trustAllCerts,new SecureRandom());
        sslSocketFactory = sslContext.getSocketFactory();
        sslSocket = (SSLSocket)sslSocketFactory.createSocket(SERVER_ADDR, Integer.parseInt(SERVER_PORT));
        sslSocket.setTcpNoDelay(true);
        dataInputStream = new DataInputStream(sslSocket.getInputStream());
        dataOutputStream = new DataOutputStream(sslSocket.getOutputStream());
    }
    public boolean transmitAuthMessage(String email,String password) throws IOException {

        dataOutputStream.writeUTF(RequestCodes.AUTH_REQUEST);
        dataOutputStream.flush();

        if(!dataInputStream.readUTF().equals(RequestCodes.CONTINUE)){
            throw new IOException("Server Error");
        }
        String credentials = formatAuthDetails(email,password);
        dataOutputStream.writeUTF(credentials);
        dataOutputStream.flush();

        String authResponse = dataInputStream.readUTF();
    System.out.println(authResponse);
        if(authResponse.equals(RequestCodes.AUTH_FAIL)){
            return false;
        }
        loggedIn = true;
        dataInputStream.close();
        dataOutputStream.close();
        sslSocket.close();
        return true;
    }
    public ArrayList<String> requestEncryptionKeys() throws IOException {
        ArrayList<String> encryptionKeys = new ArrayList<>(3);
        dataOutputStream.writeUTF(RequestCodes.ENCRYPTION_KEY_REQUEST);
        dataOutputStream.flush();

        String serverResp = dataInputStream.readUTF();
        if(!serverResp.equals(RequestCodes.CONTINUE)){
            throw new IOException("Server Error");
        }
        dataOutputStream.writeUTF(RequestCodes.KEY_REQUEST);
        dataOutputStream.flush();

        String pkResponse = dataInputStream.readUTF();
        if(!pkResponse.equals(RequestCodes.KEY_GOOD)){
            throw new IOException("Couldn't get a key");
        }
        String Key = dataInputStream.readUTF();
    System.out.println("K:" + Key.length());
        encryptionKeys.add(Key);

        dataOutputStream.writeUTF(RequestCodes.IV_REQUEST);
        dataOutputStream.flush();

        String ivResponse = dataInputStream.readUTF();
        if(!ivResponse.equals(RequestCodes.IV_GOOD)){
            throw new IOException("Couldn't get Initialization Vector");
        }
        String initializationVector = dataInputStream.readUTF();
        System.out.println("iv:" + initializationVector.length());
        encryptionKeys.add(initializationVector);
        //Address:port;
        String confParams = dataInputStream.readUTF();
        encryptionKeys.add(confParams);
        dataOutputStream.close();
        dataInputStream.close();
        sslSocket.close();
        return encryptionKeys;
    }
    private String formatAuthDetails(String email,String pass){
        return email+":"+pass+";";
    }
}
