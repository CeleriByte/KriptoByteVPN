package com.example.android.KriptoByteVPN;

/**
 * Created by ziscky on 6/15/15.
 */
public class RequestCodes {
    public static String AUTH_REQUEST = "20";
    public static String AUTHENTICATED = "202";
    public static String AUTH_FAIL = "204";
    public static String ENCRYPTION_KEY_REQUEST  ="60";
    public static String KEY_REQUEST = "601";
    public static String KEY_GOOD = "602";
    public static String IV_REQUEST = "701";
    public static String IV_GOOD = "702";
    public static String DATA_ERR = "604";
    public static String CONTINUE = "101";
}
