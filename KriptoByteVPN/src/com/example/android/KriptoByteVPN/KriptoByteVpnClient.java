
package com.example.android.KriptoByteVPN;

import android.app.Activity;
import android.content.Intent;
import android.net.VpnService;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.example.android.toyvpn.R;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

public class KriptoByteVpnClient extends Activity implements View.OnClickListener {
    private TextView mEmail;
    private TextView mPass;
    private TextView loading;
    private ProgressBar progressBar;
    private Button loginButton;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        mEmail = (TextView) findViewById(R.id.email);
        mPass = (TextView) findViewById(R.id.pass);
        loading = (TextView)findViewById(R.id.textLoad);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        loading.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
        loginButton = (Button)findViewById(R.id.login);
        loginButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(mEmail.getText().length() == 0 || mPass.getText().length() == 0){
            Toast.makeText(this,"Please Fill all input fields",Toast.LENGTH_SHORT).show();
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        loading.setVisibility(View.VISIBLE);

        new ParallelAuthentication(getApplicationContext(),progressBar,loading,loginButton).execute(mEmail.getText().toString(),mPass.getText().toString());


    }

}
