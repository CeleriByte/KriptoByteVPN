package com.example.android.KriptoByteVPN;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.math.BigInteger;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

/**
 * Created by ziscky on 6/15/15.
 */
public class RSA {
    private RSAPublicKey rsaPublicKey;
    private RSAPrivateKey rsaPrivateKey;
    KeyFactory keyFactory;
    public RSA(RSAPrivateKey rsaPrivateKey,RSAPublicKey rsaPublicKey) throws NoSuchAlgorithmException {
        this.rsaPublicKey = rsaPublicKey;
        this.rsaPrivateKey = rsaPrivateKey;
        keyFactory = KeyFactory.getInstance("RSA");
    }
    public byte[] Encrypt(byte[] packet) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE,rsaPublicKey);
        byte[] encryptedPacket = cipher.doFinal(packet);
        return encryptedPacket;
    }
    public byte[] Decrypt(byte[] packet) throws InvalidKeySpecException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE,rsaPrivateKey);
        byte[] decryptedPacket = cipher.doFinal(packet);
        return decryptedPacket;
    }

}
