package com.example.android.KriptoByteVPN;

import android.app.Activity;
import android.content.Intent;
import android.net.VpnService;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.example.android.toyvpn.R;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by ziscky on 6/15/15.
 */
public class VpnConnect extends Activity implements View.OnClickListener{
    private Button button;
    private Button buttonStart;
    private TextView textView;
    private ProgressBar progressBar;
    private ArrayList<String> encryptionKeys;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form);
        encryptionKeys = new ArrayList<>(3);//publicKey,privateKey,confParams
        button = (Button)findViewById(R.id.connect);
        buttonStart = (Button)findViewById(R.id.buttonStart);
        textView = (TextView)findViewById(R.id.textEMess);
        progressBar = (ProgressBar)findViewById(R.id.progressBarE);
        button.setOnClickListener(this);
        buttonStart.setOnClickListener(this);
        progressBar.setVisibility(View.INVISIBLE);
        textView.setVisibility(View.INVISIBLE);
        buttonStart.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onClick(View v) {
    System.out.println(R.id.connect);
        switch (v.getId()){
            case R.id.connect:
                new ParallelKeyExchange(getApplicationContext(),encryptionKeys,textView,progressBar,buttonStart,button).execute();
                break;
            case R.id.buttonStart:
                startVpnService();
        }
    }
    public void startVpnService(){
        if(encryptionKeys.get(0) == null || encryptionKeys.get(1) == null ){
            textView.setText("Failed To Get Encryption Keys");
            progressBar.setVisibility(View.INVISIBLE);
        }
        Intent intent = VpnService.prepare(this);
        if (intent != null) {
            startActivityForResult(intent, 0);
        } else {
            onActivityResult(0, RESULT_OK, null);
        }
    }
    @Override
    protected void onActivityResult(int request, int result, Intent data) {
        if (result == RESULT_OK) {
            String prefix = getPackageName();
            String[] conf = null;
            try {
                conf = unpackParams(encryptionKeys.get(2));
            } catch (IOException e) {
                e.printStackTrace();
            }
        System.out.println(conf[0]+":"+conf[1]);
            Intent intent = new Intent(this, KriptoByteVpnService.class).putExtra(prefix + ".KEY", encryptionKeys.get(0))
                    .putExtra(prefix + ".IV", encryptionKeys.get(1)).putExtra(prefix + ".ADDRESS",conf[0])
                    .putExtra(prefix + ".PORT",conf[1]);
            startService(intent);
        }
    }
    private String[] unpackParams(String confStr) throws IOException{
        String[] params = confStr.split(":");
        if(!params[1].endsWith(";")){
            throw new IOException("Received Parameter Error");
        }
        String[] indiv = new String[2];
        indiv[0] = params[0];//Address
        indiv[1] = params[1].replace(";","");//Port
        return indiv;
    }
}
